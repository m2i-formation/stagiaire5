package org.m2iformation.dolibarr.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeDriver;

public class Connexion {
	
	public static ChromeDriver driver;
	
	
	//va se lancer une fois avant la classe
	@BeforeClass
	public static void setupClass() {
		//on initialise l objet driver
		driver = new ChromeDriver();
		// on lui dit d attendre 20 secondes si l il y a un pb r�seau lorsque le script s execute
		driver.manage().timeouts().implicitlyWait(20,TimeUnit.SECONDS);
		
	}
	
	
	//se lance � chaque
	@Before
	public void setup() {
		//lancement � l'adresse
	}
	
	
	@Test
	public void testConnexion() throws InterruptedException {
		//les test et verif du test
		driver.get("http://demo.testlogiciel.pro/dolibarr/");
		driver.findElement(By.id("username")).sendKeys("jsmith");
		driver.findElement(By.name("password")).sendKeys("dolibarrhp");
		driver.findElement(By.xpath("//input[contains(@value,'Connexion')]")).click();//pas besoin de regexp on utilise contains
		
		//on lui dit d attendre 5 secondes apres le clic, 
		//la methode renvoie 1 exception donc ajoute throw declaration � methode
		Thread.sleep(5000);
		
		assertTrue(driver.findElementByClassName("titre").isDisplayed());
		assertEquals("Espace accueil", driver.findElement(By.className("titre")).getText());
		driver.findElement(By.xpath("//img[@alt='D�connexion']")).click();
	}
	
	
	@After
	public void tearDown() {
		
	}
	
	
	@AfterClass
	public static void tearDownClass() {
		//on quitte le driver
		//driver.quit();
		
	}
	
}
